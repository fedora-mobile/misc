Name:           kernel-ce
Version:        5.9
Release:        1%{?dist}
Summary:        latest megi kernel for the pinephone ce

License:        GPLv3+
URL:            https://megous.com/git/linux/log/?h=orange-pi-5.9
Source0:        https://xff.cz/kernels/%{version}/pp2.tar.gz

%description
Megis version of the Linux Kernel, for PinePhone CE.

%global debug_package %{nil}

%prep
%autosetup -p1 -n pp2-%{version}

%install
mkdir -p $RPM_BUILD_ROOT/boot/
cp board.itb $RPM_BUILD_ROOT/boot/
mkdir -p $RPM_BUILD_ROOT/lib/modules/
cp -r modules/lib/modules/* $RPM_BUILD_ROOT/lib/modules/

%files
/boot/board.itb
/lib/modules/

%changelog
* Fri Aug 21 2020 Torrey Sorensen <sorensentor@tuta.io> - 5.9-1
- Upgrade to 5.9

* Thu Aug 06 2020 Nikhil Jha <hi@nikhiljha.com> - 5.8-1
- Upgrade to 5.8

* Mon Jun 15 2020 Nikhil Jha <hi@nikhiljha.com> - 5.7-1
- Initial packaging (hack)

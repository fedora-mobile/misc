Name:           lightdm-mobile-greeter
Version:        5
Release:        1%{?dist}
Summary:        a touch-optimized lightdm greeter

License:        GPLv3+
URL:            https://source.puri.sm/Librem5/squeekboard
Source0:        https://raatty.club:3000/raatty/lightdm-mobile-greeter/archive/%{version}.tar.gz
Source1:        60-lightdm-mobile-greeter.conf

ExclusiveArch:  %{rust_arches}

BuildRequires:  rust
BuildRequires:  cargo
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(cairo-gobject)
BuildRequires:  pkgconfig(liblightdm-gobject-1)
BuildRequires:  pkgconfig(pango)
BuildRequires:  pkgconfig(atk)
BuildRequires:  pkgconfig(gdk-pixbuf-2.0)
BuildRequires:  pkgconfig(gdk-3.0)
BuildRequires:  pkgconfig(libhandy-0.0)

Requires:  lightdm
Provides:  lightdm-greeter = 1.2

%description
This is a greeter that works on phones.

%global debug_package %{nil}

%prep
%autosetup -p1 -n lightdm-mobile-greeter

%build
%make_build


%install
# manually do a make install because it hardcodes bad install paths otherwise
install -Dm755 target/release/lightdm-mobile-greeter -t $RPM_BUILD_ROOT/usr/bin
install -Dm644 lightdm-mobile-greeter.desktop -t $RPM_BUILD_ROOT/usr/share/xgreeters
install -Dm644 %{SOURCE1} -t $RPM_BUILD_ROOT/usr/share/lightdm/lightdm.conf.d

%files
/usr/bin/lightdm-mobile-greeter
/usr/share/xgreeters/lightdm-mobile-greeter.desktop
/usr/share/lightdm/lightdm.conf.d/60-lightdm-mobile-greeter.conf


%changelog
* Sun Sept 27 2020 Torrey Sorensen <sorensentor@tuta.io> - 5-1
- Update to version 5

* Mon Jun 15 2020 Nikhil Jha <hi@nikhiljha.com> - 3-2
- Add configuration (still a hack)

* Sat Jun 13 2020 Nikhil Jha <hi@nikhiljha.com> - 3-1
- Initial packaging (hack)
